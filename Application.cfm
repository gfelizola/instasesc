<cfsetting enablecfoutputonly="yes">
<cfapplication name="redessociais" sessionmanagement="yes" sessiontimeout="#CreateTimeSpan(0,4,0,0)#">

<cfif not structKeyExists(application,"init")>
	<cfinvoke component="/sesc/hotsites/redes_sociais/componentes/Application" method="init">
		<cfinvokeargument name="ApplicationName" value="redessociais">
	</cfinvoke>
</cfif>

<cfset DSN 				= application.config.dsn>
<cfset DIR_COMPONENTES 		= application.config.dir_componentes>
<cfset DIR_APP 			= application.config.dir_app>
<cfset DIR_FISICO			= application.config.dir_fisico>
<cfset DIR_VIRTUAL			= application.config.dir_virtual>
<cfset DIR_VIRTUAL_ADMIN		= application.config.dir_virtual_admin>
<cfset ENTER				= chr(13) & chr(10)>
<cfset DIR_FISICO_UPLOAD		= DIR_FISICO & "admin\upload\">
<cfsetting enablecfoutputonly="no">