(function($){

    // $(document).foundationAlerts();
    // $(document).foundationButtons();
    // $(document).foundationAccordion();
    // $(document).foundationNavigation();
    // $(document).foundationCustomForms();
    // $(document).foundationMediaQueryViewer();
    // $(document).foundationTabs({callback:$.foundation.customForms.appendCustomMarkup});

    // $(document).tooltips();
    // $('input, textarea').placeholder();

    // UNCOMMENT THE LINE YOU WANT BELOW IF YOU WANT IE8 SUPPORT AND ARE USING .block-grids
    // $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
    // $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
    // $('.block-grid.four-up>li:nth-child(4n+1)').css({clear: 'both'});
    // $('.block-grid.five-up>li:nth-child(5n+1)').css({clear: 'both'});

    var btMais                  = $('.bt-carregar-mais');
    var btVoltar                = $('.bt-voltar');
    var imgLoading              = $('.loading-gif');
    var insta_container         = $(".container-fotos"),
        insta_next_url          = null,
        insta_client_id         = '9daf11bd64054bba9335e92e496bf22c' ;

    var listaDataTpl            = '';// '<div class="bloco-data">{0}</div>';
    var listaFotosTpl           = '<li>' ;
        listaFotosTpl           +=   '<a href="{1}" class="imagem"><img src="{0}" /></a>' ;
        listaFotosTpl           +=   '<div class="legenda">' ;
        listaFotosTpl           +=       '<p><a href="{3}" class="usuario" target="_blank">{2}</a> {4}</p>' ;
        listaFotosTpl           +=   '</div>' ;
        listaFotosTpl           += '</li>';
    var containerFotosTpl       = '<ul class="fotos">{0}</ul>';
    var containerBlocoTpl       = '<div class="row"><div class="twelve columns">{0}</div></div>';
    var containerInternaFotoTpl = '<img src="{0}" width="100%" alt="" />';
    var containerDescricaoTpl   = '<p><a href="{1}" target="_blank">@{0}</a> {2}</p>';

    var hashAtual               = 'sescsp' ;
    var userAtual               = null ;
    var sescUserId              = '191171247' ;
    var sescHash                = 'sescsp' ;

    function carregarInstagram (e) {
        e = e || event ;
        if(e) e.preventDefault();

        console.log( 'hashAtual', hashAtual, 'userAtual', userAtual );

        if( hashAtual != null ){
            $('.tags-sesc .tag').text('#' + hashAtual);
        }

        $.fn.instagram({
            hash               : hashAtual,
            userId             : userAtual,
            clientId           : insta_client_id,
            image_size         : 'low_resolution',
            photoLink          : false,
            next_url           : insta_next_url,
            onComplete         : function (photos, data) {
                insta_next_url = data.pagination.next_url ;
                montaListaFotos(photos);
                btMais.css({display:'block'});
                imgLoading.hide();
            },
            onLoad: function() {
                btMais.css({display:'none'});
                imgLoading.show();
            }
        });
    };

    function formataData (data) {
        var dia      = String(data.getDate());
        var mes      = String(data.getMonth() + 1);
        var ano      = data.getFullYear();

        while(dia.length < 2) dia = '0' + dia ;
        while(mes.length < 2) mes = '0' + mes ;

        return dia + '/' + mes + '/' + ano;
    }

    function montaListaFotos(fotos){
        var blocos = [];
        var nomesDias = [];
        $.each(fotos, function(index, foto) {
            var fotoData = new Date( Number( foto.created_time ) * 1000 );
            var fotoDataFormatada = formataData(fotoData);

            if( blocos[fotoDataFormatada] ){
                blocos[fotoDataFormatada].push( foto );
            } else {
                nomesDias.push( fotoData );
                blocos[fotoDataFormatada] = [foto] ;
            }
        });

        //ordena as datas
        nomesDias.sort(function(a,b){ return b-a; });
        var htmlCompleto = '';
        var htmlLista = '';
        var htmlBloco = listaDataTpl.format(b);

        for (var j = 0; j < nomesDias.length; j++) {
            var b = formataData(nomesDias[j]);
            // 

            for (var i = 0; i < blocos[b].length; i++) {
                var f = blocos[b][i];

                if( f ){
                    var foto = f.images ? f.images.low_resolution.url : '';
                    var usuario = f.user ? f.user.username : '';
                    var legenda = f.caption ? f.caption.text : '';

                    htmlLista += listaFotosTpl.format( foto, '#foto/' + f.id, '@' + usuario, 'http://instagram.com/' + usuario, legenda);
                }

            };
        }

        htmlBloco += containerFotosTpl.format( htmlLista );
        htmlCompleto += containerBlocoTpl.format( htmlBloco );

        $('.container-fotos').append(htmlCompleto);
        $('.container-fotos ul li').click(function(e) {
            e = e || event;
            if(e) e.preventDefault();
            carregaInterna($(this).find('.imagem').attr('href').substr(6));
        });
    }

    function carregaInterna (fotoId) {
        $('.conteudo-principal').fadeOut();
        $('.conteudo-interna').fadeIn();
        $('.conteudo-interna .loading-interna').fadeIn();

        $('.conteudo-interna .conteudo-foto-interna').empty();
        $('.conteudo-interna .descricao').empty();

        $('.fb-comments').attr('data-href', 'http://www.sescsp.org.br/sesc/hotsites/redes_sociais/instasesc/?fid=' + fotoId)
        $('.fb-like').attr('data-href', 'http://www.sescsp.org.br/sesc/hotsites/redes_sociais/instasesc/?fid=' + fotoId)

        $.ajax({
            type    : "GET",
            dataType: "jsonp",
            cache   : false,
            url     : 'https://api.instagram.com/v1/media/' + fotoId + '?client_id=9daf11bd64054bba9335e92e496bf22c',
            success : function (res) {
                console.log(res);

                $('.conteudo-interna .loading-interna').fadeOut();
                $('.conteudo-interna .conteudo-foto-interna').html( containerInternaFotoTpl.format( res.data.images.standard_resolution.url ) );
                $('.conteudo-interna .descricao').html( containerDescricaoTpl.format(
                    res.data.user.username,
                    'http://instagram.com/' + res.data.user.username,
                    res.data.caption.text
                ));

                try{
                    FB.XFBML.parse();
                }catch(ex){}
            }
        });
    }

    function limparCarregar() {
        $('.container-fotos').empty();
        insta_next_url = null;
        carregarInstagram();
    }

    function voltaHome (e) {
        e = e || event ;
        if(e) e.preventDefault();

        $('.conteudo-interna').fadeOut('slow', function() {
            $('.conteudo-principal').fadeIn();
        });
    }

    $.datepicker.regional['pt-BR'] = {
        closeText         : 'Fechar',
        prevText          : '&#x3C;Anterior',
        nextText          : 'Próximo&#x3E;',
        currentText       : 'Hoje',
        monthNames        : ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
        monthNamesShort   : ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
        dayNames          : ['Domingo','Segunda-feira','Terça-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sábado'],
        dayNamesShort     : ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb'],
        dayNamesMin       : ['DOM','SEG','TER','QUA','QUI','SEX','SÁB'],
        weekHeader        : 'Sm',
        dateFormat        : 'dd/mm/yy',
        firstDay          : 0,
        isRTL             : false,
        showMonthAfterYear: false,
        yearSuffix        : ''
    };
    $.datepicker.setDefaults($.datepicker.regional['pt-BR']);

    // calendário
    $('.calendario-container').datepicker({
        showOtherMonths  : true,
        selectOtherMonths: true,
        maxDate          : "-1D",
        onSelect         : function(date) {
            // alert(date);
        }
    });

    btMais.click(carregarInstagram);
    btVoltar.click(voltaHome);

    // menu
    $('.menu .item-menu').click(function(e) {


        if( $(this).hasClass('unidade') ){
            $(this).toggleClass('active');
            $('.menu .sub-menu.menu-unidades').fadeIn('400');
            $('.menu .sub-menu.menu-data').fadeOut('400');
            $('.menu .sub-menu.menu-eventos').fadeOut('400');
        }

        if( $(this).hasClass('data') ){
            $(this).toggleClass('active');
            $('.menu .sub-menu.menu-data').fadeToggle('400');
            $('.menu .sub-menu.menu-unidades').fadeOut('400');
            $('.menu .sub-menu.menu-eventos').fadeOut('400');
        }

        if( $(this).hasClass('eventos-especiais') ){
            $(this).toggleClass('active');
            $('.menu .sub-menu.menu-eventos').fadeToggle('400');
            $('.menu .sub-menu.menu-unidades').fadeOut('400');
            $('.menu .sub-menu.menu-data').fadeOut('400');
        }

        if( $(this).hasClass('hashsesc') ){
            $('.menu .item-menu').removeClass('active');
            $(this).addClass('active');
            $('.menu .sub-menu').fadeOut('400');

            hashAtual = sescHash;
            limparCarregar();
        }

        e = e || event ;
        if(e){
            e.preventDefault();
            e.stopImmediatePropagation();
        }
    });

    $(document).click(function(e) {
        $('.menu .sub-menu').fadeOut('400');
    });

    $('.menu .sub-menu ul li a').click(function(e) {
        e = e || event ;
        if(e) e.preventDefault();

        hashAtual = $(this).attr('href').substr(1);
        limparCarregar();

        $('.menu .item-menu').removeClass('active');
        if( $(this).parents('.sub-menu').hasClass('menu-unidades') ){
            $('.menu .item-menu.unidade').addClass('active');
        } else if( $(this).parents('.sub-menu').hasClass('menu-eventos') ){
            $('.menu .item-menu.eventos-especiais').addClass('active');
        }
    });

    carregarInstagram();

})(jQuery);

function _StringFormatInline() {
    var txt = this;
    for (var i = 0; i < arguments.length; i++) {
        var exp = new RegExp('\\{' + (i) + '\\}', 'gm');
        txt = txt.replace(exp, arguments[i]);
    }
    return txt;
}
function _StringFormatStatic() {
    for (var i = 1; i < arguments.length; i++) {
        var exp = new RegExp('\\{' + (i - 1) + '\\}', 'gm');
        arguments[0] = arguments[0].replace(exp, arguments[i]);
    }
    return arguments[0];
}
if (!String.prototype.format) {
    String.prototype.format = _StringFormatInline;
}
if (!String.format) {
    String.format = _StringFormatStatic;
}
