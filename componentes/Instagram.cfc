<cfcomponent name="InstagramEvento" displayname="InstagramEvento" hint="InstagramEvento" extends="Owner" output="no">

<cffunction name="ListarPelaHashtag" returntype="struct">
		<cfargument name="nextPage" type="string" required="no">
		<cfargument name="hashtag" type="string" required="yes" >

		<cfset instagramURL = "https://api.instagram.com/v1/tags/"& #hashtag# &"/media/recent?client_id=9daf11bd64054bba9335e92e496bf22c">


		<cfif nextPage NEQ "">
			<cfset instagramURL = instagramURL & "&max_tag_id=" & #nextPage#>
		</cfif>

		<cfset structName = StructNew()>
		<cfset ListaFotos = []>


		<cfhttp url="#instagramURL#" method="get" result="jsonPhotos" />
		<cfset jsonData = deserializeJSON(jsonPhotos.fileContent) />

		<cfreturn jsonData>
</cffunction>

<cffunction name="ListarPelaHashtagSemPaginacao" returntype="struct">
		<cfargument name="hashtag" type="string" required="yes" >

		<cfset instagramURL = "https://api.instagram.com/v1/tags/"& #hashtag# &"/media/recent?client_id=9daf11bd64054bba9335e92e496bf22c">

		<cfset structName = StructNew()>
		<cfset ListaFotos = []>

		<cfhttp url="#instagramURL#" method="get" result="jsonPhotos" />
		<cfset jsonData = deserializeJSON(jsonPhotos.fileContent) />

		<cfreturn jsonData>
</cffunction>

<cffunction name="ListaEventos" returntype="query">
		<cfquery name="dados" datasource="#application.config.dsn#">
			select
				Id,
				Usuario,
				ImagemThumb,
				ImagemGrande,
				Texto,
				IdInstagram,
				Convert(VARCHAR(30),DataFoto,5),
				Hashtag,
				Link
			from Tbl_RedesSociais_CPC_Instagram
			order by DataFoto desc
		</cfquery>

		<cfreturn dados />
	</cffunction>

	<cffunction name="Listar" returntype="query">
		<cfquery name="dados" datasource="#application.config.dsn#">
			select
				Id,
				Usuario,
				ImagemThumb,
				ImagemGrande,
				Texto,
				IdInstagram,
				Convert(VARCHAR(30),DataFoto,5),
				Hashtag,
				Link
			from Tbl_RedesSociais_CPC_Instagram
			order by DataFoto desc
		</cfquery>

		<cfreturn dados />
	</cffunction>


<cffunction name="GetByInstagramID" returntype="query">
		<cfargument name="instagramID" type="string" required="yes">

		<cfquery name="dados" datasource="#application.config.dsn#">
			select *
			from Tbl_RedesSociais_CPC_Instagram
			where IdInstagram = <cfqueryparam cfsqltype="cf_sql_varchar" value="#instagramID#">

		</cfquery>

		<cfreturn dados>
	</cffunction>

	<cffunction name="Exclui">
		<cfargument name="instagramID" type="string" required="yes">

		<cfquery datasource="#application.config.dsn#">
			delete from Tbl_RedesSociais_CPC_Instagram
			where IdInstagram = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.instagramID#">
		</cfquery>
	</cffunction>


	<cffunction name="Insere">
		<cfargument name="usuario" type="string" required="yes">
		<cfargument name="thumb" type="string" required="yes">
		<cfargument name="image" type="string" required="yes">
		<cfargument name="link" type="string" required="yes">
		<cfargument name="texto" type="string" required="yes">
		<cfargument name="instagramID" type="string" required="yes">
		<cfargument name="datafoto" type="date" required="yes">
		<cfargument name="hashtag" type="string" required="yes">

		<cfquery name="insere" datasource="#application.config.dsn#">
			INSERT INTO Tbl_RedesSociais_CPC_Instagram (
					IdInstagram,
					Usuario,
					ImagemThumb,
					ImagemGrande,
					Texto,
					DataFoto,
					Hashtag,
					link
				) VALUES (
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.instagramID#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.usuario#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.thumb#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.image#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.texto#">,
					<cfqueryparam cfsqltype="cf_sql_date" value="#arguments.datafoto#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.hashtag#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.link#">
				)
		</cfquery>
	</cffunction>


</cfcomponent>